Source: tox-current-env
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Bo YU <vimer@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               pybuild-plugin-pyproject,
               python3-setuptools,
               python3-pytest <!nocheck>,
               python3-pytest-xdist <!nocheck>,
               tox <!nocheck>,
Standards-Version: 4.7.0
Homepage: https://github.com/fedora-python/tox-current-env
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/tox-current-env.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/tox-current-env/-/tree/debian/main/

Package: tox-current-env
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
         tox,
Description: tox plugin to run tests in current Python environment
 The Python ecosystem now has formal specifications for many pieces of
 package metadata like versions or dependencies. However, there is no
 standardization yet for declaring test dependencies or running
 tests. The most popular de-facto standard for that today is tox, and
 expecting a future standard to evolve from tox.ini. This plugin
 to use tox's dependency lists and testing commands for environments
 other than Python venvs.
 .
 The plugin will enable community best practices around tox
 configuration to grow to better accommodate non-virtualenv environments
 in general – for example, Linux distros, Conda, or containers.
